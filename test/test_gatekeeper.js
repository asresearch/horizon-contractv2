const TestERC20 = artifacts.require("TestERC20");
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
const getBlockNumber = require('./blockNumber')(web3)
var BigNumber = require('bignumber.js');
const { default: Big } = require('big.js');


const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");

const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");

const HGateKeeper = artifacts.require("HGateKeeper");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");

const HEnvFactory = artifacts.require("HEnvFactory");
const HEnv = artifacts.require("HEnv");
const HDispatcherFactory = artifacts.require("HDispatcherFactory");
const HDispatcher = artifacts.require("HDispatcher");

const MockYieldStreamFactory = artifacts.require("MockYieldStreamFactory");
const MockYieldStream = artifacts.require("MockYieldStream");

const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
//const TokenBank = artifacts.require("TokenBank");
//const TokenBankFactory = artifacts.require("TokenBankFactory");

contract("TestGateKeeper", (accounts) =>{
  let erc20 = {}
  let longterm_factory = {}
  let long_term = {}
  let token_factory = {}

  let gatekeeper_factory = {}
  let gatekeeper = {}

  let env = {}
  let mock_stream = {}

  let fee_pool = {}
  let fee_pool_tlist = {}

  let lt = {}
  let s1e18 = "1000000000000000000";

  let ratios = [5000000, 10000000, 20000000, 30000000, 40000000, 50000000, 0]


  context("init", ()=>{
    it("init", async() =>{
      erc20 = await TestERC20.deployed();//test target token, decimal 1e18
      //token_factory = await HTokenFactory.deployed();
      token_factory = await CachedHTokenFactory.deployed();//To not use contract pool, use the previous line

      longterm_factory = await HLongTermFactory.deployed();
      assert.ok(longterm_factory);
      gatekeeper_factory = await HGateKeeperFactory.deployed();
      assert.ok(gatekeeper_factory);

      env_factory = await HEnvFactory.deployed();

      tx = await env_factory.createHEnv(erc20.address);
      env = await HEnv.at(tx.logs[0].args.addr);
      assert.ok(env);

      dispatcher_factory = await HDispatcherFactory.deployed();
      tx = await dispatcher_factory.createHDispatcher();
      dispatcher = await HDispatcher.at(tx.logs[0].args.addr);
      assert.ok(dispatcher);

      mock_factory = await MockYieldStreamFactory.deployed();
      tx = await mock_factory.createMockYieldStream(erc20.address);
      mock_stream = await MockYieldStream.at(tx.logs[0].args.addr);
      assert.ok(mock_stream);
      await dispatcher.resetYieldStream(erc20.address, mock_stream.address);

      start_block = await getBlockNumber();
      tokentx = await longterm_factory.createLongTerm(erc20.address, start_block, 10, token_factory.address);

      long_term = await HLongTerm.at(tokentx.logs[0].args.addr);
      assert.ok(long_term);
      tlist = await TrustList.at(await token_factory.trustlist());

      tx = await gatekeeper_factory.createGateKeeperForPeriod(env.address, dispatcher.address, long_term.address);
      gatekeeper = await HGateKeeper.at(tx.logs[2].args.addr);
      await gatekeeper.resetSupportRatios([5000000, 10000000, 20000000, 30000000, 40000000, 50000000])
      await tlist.add_trusted(gatekeeper.address)


      for (i = 0; i < 6; i++){
        await long_term.getOrCreateLongTermToken(ratios[i])
        //lt_token = await HToken.at(tx.logs[0].args.addr)
        console.log("lt",i,":", await long_term.get_long_term_token_with_ratio(ratios[i]))
        //console.log("lt",i,":", lt_token.address)
        lt[i + 1] = await HToken.at(await long_term.get_long_term_token_with_ratio(ratios[i]))
        assert.ok(lt[i + 1]);
        lt[i +1].transferOwnership(gatekeeper.address)
      }
      await long_term.getOrCreateLongTermToken(0)
      console.log("lt float:", await long_term.get_long_term_token_with_ratio(0))
      lt[0] = await HToken.at(await long_term.get_long_term_token_with_ratio(0))
      assert.ok(lt[0]);
      await lt[0].transferOwnership(gatekeeper.address)

      await long_term.transferOwnership(gatekeeper.address);
    });

    it("setup pool and issue token ", async() =>{
      tlist_factory = await TrustListFactory.deployed();
      //bank_factory = await TokenBankFactory.deployed();

      tx= await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
      fee_pool_tlist = await TrustList.at(tx.logs[0].args.addr);
      assert.ok(fee_pool_tlist);
      //tx = await bank_factory.newTokenBank("ETH pool", '0x0000000000000000000000000000000000000000', fee_pool_tlist.address);
      //fee_pool = await TokenBank.at(tx.logs[0].args.addr);
      //assert.ok(fee_pool);

      //await fee_pool_tlist.add_trusted(gatekeeper.address);

      await env.changeFeePoolAddr(accounts[9]);
      //await env.changeFeePoolAddr(fee_pool.address);

      //send erc20 to accounts
      for(i = 0; i < 10; i++){
        await erc20.generateTokens(accounts[i], "100000000000000000000000000");
        b = (await erc20.balanceOf(accounts[i])).toString();
        expect(b).to.not.equal("0");
      }

      await gatekeeper.changeYieldPool(accounts[8], {from:accounts[0]});

    });

    it('one bid', async() =>{
      await expectRevert(gatekeeper.bidRatio(100, 490000), "not support ratio");
      await erc20.approve(gatekeeper.address, "1000000000000000000000000000", {from: accounts[0]})
      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round in one bid: ", period)
      console.log("one bidding...")

      await gatekeeper.bidRatio(1000, 5000000, {from:accounts[0]});

      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round after one bid: ", period)
      token1 = await long_term.hintokenAtPeriodWithRatio(period + 1, 5000000);
      hintoken1 = await HToken.at(token1);
      b = (await hintoken1.balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(1000);

      await expectRevert(gatekeeper.exchangeToLongTermToken(hintoken1.address), "GK: round not sealed");

      for(i = 0; i < 11; i++){
        await erc20.transfer(accounts[0], 0, {from:accounts[0]});
        //console.log("dfdfdf:,", i);
        console.log("block:", (await web3.eth.getBlock("latest")).number)
        await gatekeeper.updatePeriodStatus();
      }
      //expect(1).to.equal(0)
      await gatekeeper.exchangeToLongTermToken(hintoken1.address)
      //lt1addr = await long_term.get_long_term_token_with_ratio(5000000)
      //lt1 = await HToken.at(lt1addr);
      b = (await hintoken1.balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(0);
      b = (await lt[1].balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(1000);

      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round after one bid: ", period)

      await gatekeeper.withdrawLongTerm(lt[1].address,1000);

      hout1addr = await long_term.houttokenAtPeriodWithRatio(period + 1, 5000000);
      houttoken1 = await HToken.at(hout1addr);
      b = (await houttoken1.balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(1000);


      console.log("in1adddr: ", hintoken1.address)
      console.log("out1adddr: ", hout1addr)
      expect(hout1addr).to.equal(hintoken1.address);//required for contract pool
      //expect(1).to.equal(0);


      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round after withdraw: ", period)

      await expectRevert(gatekeeper.claim(houttoken1.address,1000),"GK: period not end")

      await gatekeeper.cancelWithdraw(houttoken1.address)
      b = (await houttoken1.balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(0);
      b = (await lt[1].balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(1000);

      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round before withdraw2: ", period)

      await gatekeeper.withdrawLongTerm(lt[1].address,1000);

      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round after withdraw2: ", period)

      j = (await long_term.getCurrentRoundStartBlock()).toNumber();
      console.log("current round start block: ", j)
      i = (await web3.eth.getBlock("latest")).number;
      while (i < j + 10){
        await gatekeeper.updatePeriodStatus();
        i = (await web3.eth.getBlock("latest")).number;
      }
      console.log("current block: ", i)
      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round at claim: ", period)

      prev_b = (await erc20.balanceOf(accounts[0])).toString();
      tx = await gatekeeper.claim(houttoken1.address, 1000)
      //tx = await gatekeeper.claim(htoken.address, 1000, {from:accounts[0]});
      after_b = (await erc20.balanceOf(accounts[0])).toString();

      //console.log("tx logs: ", tx.logs);
      console.log("prev_b ", prev_b, "after: ", after_b, ", should be added 1000");
    })
    /*
    it('cancel bid/ rebid', async() =>{
      await gatekeeper.bidRatio(1000, 50, {from:accounts[0]});
      period = (await long_term.getCurrentRound()).toNumber();
      token1 = await long_term.htokenAtPeriodWithRatio(period + 1, 50);
      htoken = await HToken.at(token1);
      await gatekeeper.cancelBid(htoken.address, {from:accounts[0]});
      b = (await htoken.balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(0);
    })*/

    it('multiple bid case 1', async() =>{

      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }

      await gatekeeper.bidRatio("1000000000000000000000", 5000000, {from:accounts[0]});
      await gatekeeper.bidRatio("2000000000000000000000", 10000000, {from:accounts[0]});
      await gatekeeper.bidFloating("3000000000000000000000", {from:accounts[0]});

      //console.log(tx.logs)
      round = (await long_term.getCurrentRound()).toNumber();

      console.log("bid round: ", round);

      tin1addr = await long_term.hintokenAtPeriodWithRatio(round + 1, 5000000)
      tin1 = await HToken.at(tin1addr);
      assert.ok(tin1);

      //end current period
      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 1){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("start the bidding round, round: ", (await long_term.getCurrentRound()).toNumber());
      r1 = (await lt[1].ratio_to_target()).toString();
      r2 = (await lt[2].ratio_to_target()).toString();
      r3 = (await lt[3].ratio_to_target()).toString();
      r0 = (await lt[0].ratio_to_target()).toString();

      expect(r1).to.equal(s1e18);
      expect(r2).to.equal(s1e18);
      expect(r3).to.equal(s1e18);
      expect(r0).to.equal(s1e18);

      token1 = await long_term.hintokenAtPeriodWithRatio(round + 1, 5000000);
      hintoken1 = await HToken.at(token1);
      token2 = await long_term.hintokenAtPeriodWithRatio(round + 1, 10000000);
      hintoken2 = await HToken.at(token2);
      token3 = await long_term.hintokenAtPeriodWithRatio(round + 1, 0);
      hintoken3 = await HToken.at(token3);

      b1 = (await hintoken1.ratio_to_target()).toString();
      b2 = (await hintoken2.ratio_to_target()).toString();
      b3 = (await hintoken3.ratio_to_target()).toString();

      expect(b1).to.equal(s1e18);
      expect(b2).to.equal(s1e18);
      expect(b3).to.equal(s1e18);

      await gatekeeper.updatePeriodStatus();
      await mock_stream.earn(500);

      //end the bidding period
      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 2){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }
      cp = (await long_term.getCurrentRound()).toNumber();

      //clearn the bidding
      await gatekeeper.updatePeriodStatus();

      console.log("end the bidding round, round: ", cp);

      r1 = (await lt[1].ratio_to_target()).toString();
      r1_bn = new BigNumber(r1);
      r2 = (await lt[2].ratio_to_target()).toString();
      r2_bn = new BigNumber(r2);
      r3 = (await lt[3].ratio_to_target()).toString();
      r3_bn = new BigNumber(r3);
      r0 = (await lt[0].ratio_to_target()).toString();
      r0_bn = new BigNumber(r0);

      console.log("r1: ", r1, ", r2: ", r2, ", r3: ", r3, "r0: ", r0);

      e = new BigNumber(s1e18)
      e2 = new BigNumber("1047619047619047619")
      e0 = new BigNumber("968253968253968253")

      assert(r1_bn.minus(e).absoluteValue().isLessThan(10000))
      assert(r2_bn.minus(e2).absoluteValue().isLessThan(10000))
      assert(r3_bn.minus(e).absoluteValue().isLessThan(10000))
      assert(r0_bn.minus(e0).absoluteValue().isLessThan(10000))
    });

    it('multiple bid case 2', async() =>{

      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }

      round = (await long_term.getCurrentRound()).toNumber();
      console.log("new bid round: ", round);

      await gatekeeper.bidRatio("1000000000000000000000", 5000000, {from:accounts[0]});
      await gatekeeper.bidRatio("2000000000000000000000", 10000000, {from:accounts[0]});
      await gatekeeper.bidFloating("3000000000000000000000", {from:accounts[0]});

      token1 = await long_term.hintokenAtPeriodWithRatio(round + 1, 5000000);
      hintoken1 = await HToken.at(token1);
      token2 = await long_term.hintokenAtPeriodWithRatio(round + 1, 10000000);
      hintoken2 = await HToken.at(token2);
      token3 = await long_term.hintokenAtPeriodWithRatio(round + 1, 0);
      hintoken3 = await HToken.at(token3);

      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 1){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("biding round start, round: ", round + 1);

      await gatekeeper.updatePeriodStatus();
      await mock_stream.earn(1000);

      b1 = (await hintoken1.ratio_to_target()).toString();
      b2 = (await hintoken2.ratio_to_target()).toString();
      b3 = (await hintoken3.ratio_to_target()).toString();

      assert(BigNumber(b1).minus(BigNumber(s1e18)).absoluteValue().isLessThan(10000))
      assert(BigNumber(b2).minus(BigNumber("954545454545454545")).absoluteValue().isLessThan(10000))
      assert(BigNumber(b3).minus(BigNumber("1032786885245901639")).absoluteValue().isLessThan(10000))

      console.log("b1:", b1);
      console.log("b2:", b2);
      console.log("b3:", b3);

      //end current period
      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 2){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("biding round end: ", round + 2);

      //clearn the bidding
      await gatekeeper.updatePeriodStatus();

      r1 = (await lt[1].ratio_to_target()).toString();
      r2 = (await lt[2].ratio_to_target()).toString();
      r3 = (await lt[3].ratio_to_target()).toString();
      r0 = (await lt[0].ratio_to_target()).toString();
      e1 = "954545454545454545";
      e2 = "1047619047619047619";
      e0 = "983161104128846064";
      assert(BigNumber(r1).minus(BigNumber(e1)).absoluteValue().isLessThan(10000))
      assert(BigNumber(r2).minus(BigNumber(e2)).absoluteValue().isLessThan(10000))
      assert(BigNumber(r3).minus(BigNumber(s1e18)).absoluteValue().isLessThan(10000))
      assert(BigNumber(r0).minus(BigNumber(e0)).absoluteValue().isLessThan(10000))

      console.log("r1: ", r1, ", r2: ", r2, ", r3: ", r3, "r0: ", r0);

    });
    it('multiple bid case 3', async() =>{

      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      round = (await long_term.getCurrentRound()).toNumber();
      console.log("new bid round: ", round);

      await gatekeeper.bidRatio("1000000000000000000000", 5000000, {from:accounts[0]});
      await gatekeeper.bidRatio("2001000000000000000000", 10000000, {from:accounts[0]});
      await gatekeeper.bidRatio("2003000000000000000000", 20000000, {from:accounts[0]});
      await gatekeeper.bidRatio("2008000000000000000000", 30000000, {from:accounts[0]});
      //await gatekeeper.bidFloating(3000, {from:accounts[0]});

      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 1){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("biding round start, round: ", round + 1);
      //start the bidding period
      await gatekeeper.updatePeriodStatus();
      await mock_stream.earn(1000);

      //end the bidding period
      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 2){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("biding round end: ", round + 2);
      //clearn the bidding
      await gatekeeper.updatePeriodStatus();
      r1 = (await lt[1].ratio_to_target()).toString();
      r2 = (await lt[2].ratio_to_target()).toString();
      r3 = (await lt[3].ratio_to_target()).toString();
      r4 = (await lt[4].ratio_to_target()).toString();
      r0 = (await lt[0].ratio_to_target()).toString();
      console.log("r1: ", r1, ", r2: ", r2, ", r3: ", r3, ", r4: ", r4, "r0: ", r0);
      e1 = "911157024793388429";
      assert(BigNumber(r1).minus(BigNumber(e1)).absoluteValue().isLessThan(10000))
      console.log("the folowing is too complex to test")
    });
    it('multiple bid case 4', async() =>{

      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      round = (await long_term.getCurrentRound()).toNumber();
      console.log("new bid round: ", round);


      await gatekeeper.bidRatio("1000000000000000000000", 50000000, {from:accounts[0]});
      await gatekeeper.bidRatio(1, 50000000, {from:accounts[0]});
      await gatekeeper.bidRatio(2, 20000000, {from:accounts[0]});
      await gatekeeper.bidRatio(2, 20000000, {from:accounts[0]});
      //await gatekeeper.bidFloating(3000, {from:accounts[0]});

      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 1){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("biding round start, round: ", round + 1);
      //start the bidding period
      await gatekeeper.updatePeriodStatus();
      await mock_stream.earn(5000);

       //end the bidding period
      cp = (await long_term.getCurrentRound()).toNumber();
      while(cp < round + 2){
        await gatekeeper.updatePeriodStatus();
        cp = (await long_term.getCurrentRound()).toNumber();
      }

      console.log("biding round end: ", round + 2);
      //clearn the bidding
      await gatekeeper.updatePeriodStatus();
      r1 = (await lt[1].ratio_to_target()).toString();
      r2 = (await lt[2].ratio_to_target()).toString();
      r3 = (await lt[3].ratio_to_target()).toString();
      r4 = (await lt[4].ratio_to_target()).toString();
      r6 = (await lt[6].ratio_to_target()).toString();
      r0 = (await lt[0].ratio_to_target()).toString();
      console.log("r1: ", r1, ", r2: ", r2, ", r3: ", r3, ", r4: ", r4, ", r6: ", r6, ", r0: ", r0);

      assert(BigNumber(r6).minus(BigNumber(s1e18)).absoluteValue().isLessThan(10000))

    });
    it('exchange and withdraw', async() =>{
      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      round = (await long_term.getCurrentRound()).toNumber();
      console.log("new round: ", round);

      for (i = 0; i <= round; i++)
        for (j = 0; j <= 6; j++){
          addr = await long_term.hintokenAtPeriodWithRatio(i, ratios[j])
          if (addr != "0x0000000000000000000000000000000000000000"){
            console.log("in token round ",i," ratio ", j, " :", addr)
            hintoken = await HToken.at(addr);
            bal = (await hintoken.balanceOf(accounts[0])).toString()
            if (bal != "0"){
              await gatekeeper.exchangeToLongTermToken(addr);
            }
          }
      }
      cp = (await long_term.getCurrentRound()).toNumber();

      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      round = (await long_term.getCurrentRound()).toNumber();
      console.log("new round: ", round);
      out_addr = {}
      for (j = 0; j <= 6; j++){
        bal = (await lt[j].balanceOf(accounts[0])).toString();
        if (j == 0){
          console.log("balance of long term floating: ", bal)
        }
        else{
          console.log("balance of long term ratio ", ratios[j - 1],": ", bal)
        }
        tx = await gatekeeper.withdrawLongTerm(lt[j].address, bal)
        //console.log(tx)
        out_addr[j] = tx.logs[3].args.lp_token_addr
        console.log("outaddr:", j, " ", out_addr[j])

        cp = (await long_term.getCurrentRound()).toNumber();
      }
      cp = (await long_term.getCurrentRound()).toNumber();

      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      await gatekeeper.updatePeriodStatus();
      cp = (await long_term.getCurrentRound()).toNumber();
      console.log("round at claim: ", cp)
      cp = (await long_term.getCurrentRound()).toNumber();
      for (j = 0; j <= 6; j++){
        outt = await HToken.at(out_addr[j])
        bal = await outt.balanceOf(accounts[0])
        await gatekeeper.claim(out_addr[j], bal)
      }
      bal = await erc20.balanceOf(accounts[0])
      console.log("Because of accuracy issus, a small amount of target token may loss")
      console.log("final erc20 balance:", bal.toString())
      assert(BigNumber(bal).minus(BigNumber("100000000000000000000000000")).absoluteValue().isLessThan(100000000))
    });
  //*/
  })


})
