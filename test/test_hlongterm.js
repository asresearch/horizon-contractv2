const TestERC20 = artifacts.require("TestERC20");
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
//var web3 = require('web3');

const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");

const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");

const getBlockNumber = require('./blockNumber')(web3)

function keccak256_enpacked(str) {
  k = web3.utils.soliditySha3(
            {t:"string", v:str});
  return toBuffer(k);
}

contract("TestHLongTerm", (accounts) =>{
  let erc20 = {}

  let longterm_factory = {}
  let long_term = {}
  let token_factory = {}

  context('init', ()=>{
    it('init', async() =>{
      erc20 = await TestERC20.deployed();
      token_factory = await HTokenFactory.deployed();
      longterm_factory = await HLongTermFactory.deployed();
      assert.ok(longterm_factory);

      start_block = await getBlockNumber()
      tokentx = await longterm_factory.createLongTerm(erc20.address, start_block, 10, token_factory.address);
      long_term = await HLongTerm.at(tokentx.logs[0].args.addr);
      assert.ok(long_term);
      console.log("start_block: ", start_block)
    });

    it('basic create token', async() =>{
      tx = await long_term.getOrCreateInToken(10);
      round = (await long_term.getCurrentRound()).toNumber();
      console.log("round: ", round);

      token1 = await long_term.hintokenAtPeriodWithRatio(round + 1, 10);
      await long_term.getOrCreateInToken(10);
      round = (await long_term.getCurrentRound()).toNumber();
      token2 = await long_term.hintokenAtPeriodWithRatio(round + 1, 10);

      assert.equal(token1, token2);
      token = token1

      console.log('token: ', token)

      htoken = await HToken.at(token);
      assert.equal(round + 1, await htoken.extra(keccak256_enpacked("round")))
      assert.equal(10, await htoken.extra(keccak256_enpacked("ratio")));
      assert.equal(0, await htoken.ratio_to_target());

      b = await long_term.isPeriodInTokenValid(token);
      assert.equal(b, true);

      supply1 = (await long_term.totalInAtPeriodWithRatio(round + 1, 10)).toNumber();
      supply2 = (await htoken.totalSupply()).toNumber();
      assert.equal(supply1, supply2)

      i = 0;

      while(i < 20){
        //Ganache will increase block number for each transaction
        await erc20.transfer(accounts[0], 0, {from:accounts[0]});
        i = i+1;
      }
      console.log("current block: ", await getBlockNumber());
      await long_term.updatePeriodStatus();
      i = 0;
      while(i < 2){
        await erc20.transfer(accounts[0], 0, {from:accounts[0]});
        i = i + 1;
      }

      await long_term.getOrCreateInToken(10);
      round = (await long_term.getCurrentRound()).toNumber();
      console.log("new round, ", round)
      token3 = await long_term.hintokenAtPeriodWithRatio(round + 1, 10);
      expect(token3).to.not.equal(token);
    })
  })
})
