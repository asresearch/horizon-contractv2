const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
const getBlockNumber = require('./blockNumber')(web3)

const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const TestHTokenCache = artifacts.require("TestHTokenCache");


contract('TESTHToken', (accounts) =>{


  let cache = {}
  context('init', ()=>{
    it('init', async ()=>{
      cache = await TestHTokenCache.deployed();
  })


    it('basic', async() =>{
      tx = await cache.createHToken("tst", "sym");
      addr = tx.logs[0].args.addr
      console.log("create: ", addr)
      await cache.destroyHToken(addr);

      tx = await cache.createHToken("tkt", "sms");
      assert.equal(addr, tx.logs[0].args.addr, "expect reuse");
      await cache.destroyHToken(addr);
    })

  })

});
