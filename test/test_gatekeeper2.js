const TestERC20 = artifacts.require("TestERC20");
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
const getBlockNumber = require('./blockNumber')(web3)
var BigNumber = require('bignumber.js');
const { default: Big } = require('big.js');


const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");

const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");

const HGateKeeper = artifacts.require("HGateKeeper");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");

const HEnvFactory = artifacts.require("HEnvFactory");
const HEnv = artifacts.require("HEnv");
const HDispatcherFactory = artifacts.require("HDispatcherFactory");
const HDispatcher = artifacts.require("HDispatcher");

const MockYieldStreamFactory = artifacts.require("MockYieldStreamFactory");
const MockYieldStream = artifacts.require("MockYieldStream");

const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const TokenBank = artifacts.require("TokenBank");
const TokenBankFactory = artifacts.require("TokenBankFactory");

contract("TestGateKeeper2", (accounts) =>{
  let erc20 = {}
  let longterm_factory = {}
  let long_term = {}
  let token_factory = {}

  let gatekeeper_factory = {}
  let gatekeeper = {}

  let env = {}
  let mock_stream = {}

  let fee_pool = {}
  let fee_pool_tlist = {}

  let lt = {}
  let s1e18 = "1000000000000000000";

  let ratios = [5000000, 10000000, 20000000, 30000000, 40000000, 50000000, 0]


  context("init", ()=>{
    it("init", async() =>{
      erc20 = await TestERC20.deployed();//test target token, decimal 1e18
      //token_factory = await HTokenFactory.deployed();
      token_factory = await CachedHTokenFactory.deployed();

      longterm_factory = await HLongTermFactory.deployed();
      assert.ok(longterm_factory);
      gatekeeper_factory = await HGateKeeperFactory.deployed();
      assert.ok(gatekeeper_factory);

      env_factory = await HEnvFactory.deployed();

      tx = await env_factory.createHEnv(erc20.address);
      env = await HEnv.at(tx.logs[0].args.addr);
      assert.ok(env);

      dispatcher_factory = await HDispatcherFactory.deployed();
      tx = await dispatcher_factory.createHDispatcher();
      dispatcher = await HDispatcher.at(tx.logs[0].args.addr);
      assert.ok(dispatcher);

      mock_factory = await MockYieldStreamFactory.deployed();
      tx = await mock_factory.createMockYieldStream(erc20.address);
      mock_stream = await MockYieldStream.at(tx.logs[0].args.addr);
      assert.ok(mock_stream);
      await dispatcher.resetYieldStream(erc20.address, mock_stream.address);

      start_block = await getBlockNumber();
      tokentx = await longterm_factory.createLongTerm(erc20.address, start_block, 10, token_factory.address);

      long_term = await HLongTerm.at(tokentx.logs[0].args.addr);
      assert.ok(long_term);

      tx = await gatekeeper_factory.createGateKeeperForPeriod(env.address, dispatcher.address, long_term.address);
      gatekeeper = await HGateKeeper.at(tx.logs[2].args.addr);
      await gatekeeper.resetSupportRatios([5000000, 10000000, 20000000, 30000000, 40000000, 50000000])

      tlist = await TrustList.at(await token_factory.trustlist());
      await tlist.add_trusted(gatekeeper.address)


      for (i = 0; i < 6; i++){
        await long_term.getOrCreateLongTermToken(ratios[i])
        //lt_token = await HToken.at(tx.logs[0].args.addr)
        console.log("lt",i,":", await long_term.get_long_term_token_with_ratio(ratios[i]))
        //console.log("lt",i,":", lt_token.address)
        lt[i + 1] = await HToken.at(await long_term.get_long_term_token_with_ratio(ratios[i]))
        assert.ok(lt[i + 1]);
        lt[i +1].transferOwnership(gatekeeper.address)
      }
      await long_term.getOrCreateLongTermToken(0)
      console.log("lt float:", await long_term.get_long_term_token_with_ratio(0))
      lt[0] = await HToken.at(await long_term.get_long_term_token_with_ratio(0))
      assert.ok(lt[0]);
      await lt[0].transferOwnership(gatekeeper.address)

      await long_term.transferOwnership(gatekeeper.address);
    });

    it("setup pool and issue token ", async() =>{
      //await env.changeFeePoolAddr(fee_pool.address);

      //send erc20 to accounts
      await erc20.destroyTokens(accounts[0], await erc20.balanceOf(accounts[0]))
      await erc20.generateTokens(accounts[0], 1000);
    

      await gatekeeper.changeYieldPool(accounts[8], {from:accounts[0]});
    });

    it('yield pool', async() =>{
      await erc20.approve(gatekeeper.address, 0, {from: accounts[0]})
      await erc20.approve(gatekeeper.address, "1000000000000000000000000000", {from: accounts[0]})
      period = (await long_term.getCurrentRound()).toNumber();
      console.log("round in one bid: ", period)
      console.log("one bidding...")

      await gatekeeper.bidRatio(1000, 5000000, {from:accounts[0]});
  
      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }

      hintokenaddr = await long_term.hintokenAtPeriodWithRatio(cp + 1, 5000000);
      console.log("inaddr: ", hintokenaddr)

      await gatekeeper.exchangeToLongTermToken(hintokenaddr)

      b = (await lt[1].balanceOf(accounts[0])).toNumber();
      expect(b).to.equal(1000);

      //await gatekeeper.withdrawLongTerm(lt[1].address, 1000)
      await mock_stream.earn(2000);
      //lt1addr = await long_term.get_long_term_token_with_ratio(5000000)
      //lt1 = await HToken.at(lt1addr);
      cp = (await long_term.getCurrentRound()).toNumber();

      console.log("round in stream earn: ", cp)
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      b = (await erc20.balanceOf(accounts[8])).toNumber();
      expect(b).to.equal(125);

      cp = (await long_term.getCurrentRound()).toNumber();
      console.log("round after earn: ", cp)
      //hout1addr = await long_term.houttokenAtPeriodWithRatio(cp, 5000000);
      //await gatekeeper.claim(hout1addr, 1000)

      //b = (await erc20.balanceOf(accounts[0])).toNumber();
      //expect(b).to.equal(875);
      console.log("round continue:")

      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }

      cp = (await long_term.getCurrentRound()).toNumber();
      console.log("round should revert: ", cp)
      b = (await erc20.balanceOf(accounts[8])).toNumber();
      console.log("yield balance: ", b)
      console.log("if it is 250, implies that there is a error, due to the wrong update of total_target_token_in_round[_round + 1]")
      await gatekeeper.withdrawLongTerm(lt[1].address, 1000)
      console.log("end")
    })

    it('withdraw in', async() =>{
      await erc20.generateTokens(accounts[0], 10000);
      cp = (await long_term.getCurrentRound()).toNumber();
      while(!(await long_term.isRoundEnd(cp + 1))){
        await gatekeeper.updatePeriodStatus();
      }
      cp = (await long_term.getCurrentRound()).toNumber();
      await gatekeeper.bidRatio(1000, 5000000, {from:accounts[0]});
      await gatekeeper.bidRatio(1000, 5000000, {from:accounts[0]});
      await gatekeeper.bidRatio(1000, 10000000, {from:accounts[0]});
      token1 = await long_term.hintokenAtPeriodWithRatio(cp + 1, 5000000);
      token2 = await long_term.hintokenAtPeriodWithRatio(cp + 1, 10000000);
      await gatekeeper.cancelBid(token2);

      hintoken1 = await HToken.at(token1);
      await expectRevert(gatekeeper.exchangeToLongTermToken(hintoken1.address), "GK: round not sealed");

      while(!(await long_term.isRoundEnd(cp))){
        await gatekeeper.updatePeriodStatus();
      }
      cp = (await long_term.getCurrentRound()).toNumber();
      await gatekeeper.withdrawInToken(token1, 1500, {from:accounts[0]});
      out1 = await long_term.houttokenAtPeriodWithRatio(cp + 1, 5000000);
      houttoken1 = await HToken.at(out1);

      o1 = (await houttoken1.balanceOf(accounts[0])).toNumber();
      expect(o1).to.equal(1500);
    })

  })


})
