const TestERC20 = artifacts.require("TestERC20");
const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
const getBlockNumber = require('./blockNumber')(web3)

const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");


contract('TESTHToken', (accounts) =>{

  let erc20 = {}
  let htoken = {}


  context('init', ()=>{
    it('init', async ()=>{
    erc20 = await TestERC20.deployed();
    factory = await HTokenFactory.deployed();
    assert.ok(factory)

    tokentx = await factory.createHToken("TEST1","test1", true);
    htoken = await HToken.at(tokentx.logs[0].args.addr);

  })


    it('basic', async() =>{
      let _name = await htoken.name();
      console.log("fix name: ", _name, ", symbol: ", await htoken.symbol());
    })
  })

});
