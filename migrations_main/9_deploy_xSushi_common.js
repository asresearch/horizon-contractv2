const HDispatcherFactory = artifacts.require("HDispatcherFactory");
const AddressArray = artifacts.require("AddressArray")
const SafeERC20 = artifacts.require("SafeERC20");
const SafeMath = artifacts.require("SafeMath");
const HEnvFactory = artifacts.require("HEnvFactory");
const {DHelper, StepRecorder} = require("../util.js");
const yvUSDCStream = artifacts.require("yvUSDCStream");
const MockYieldStreamFactory = artifacts.require("MockYieldStreamFactory");
const MockYieldStream = artifacts.require("MockYieldStream");
const STDERC20 = artifacts.require("STDERC20");
const HDispatcher = artifacts.require("HDispatcher");

const HEnv = artifacts.require("HEnv");

const fs = require("fs");

async function performMigration(deployer, network, accounts, dhelper) {
  if(network.includes("main")){
    dispatcher = await HDispatcher.at("0x4775D2B1A3f582b3153e8B78a5C5337036D35f54")
  }
  else{
    dispatcher_factory = await HDispatcherFactory.deployed();
    tokentx = await dispatcher_factory.createHDispatcher();
    dispatcher = await HDispatcher.at(tokentx.logs[0].args.addr);
  }

  brief = StepRecorder(network, "yvUSDC-common")
  brief.write("Dispatcher", dispatcher.address)

  if(network.includes("main")){
    stream_token = "0x8798249c2E607446EfB7Ad49eC89dD1865Ff4272"
    stream = "0x01bcA25965D57d3E305673Da484eEe22Baa4140b"
  }
  else{
    await deployer.deploy(STDERC20);
    usdt = await STDERC20.deployed();
    await deployer.link(SafeERC20, MockYieldStreamFactory);
    await deployer.link(SafeMath, MockYieldStreamFactory);
    await deployer.deploy(MockYieldStreamFactory);
    mockfactory = await MockYieldStreamFactory.deployed();
    tokentx = await mockfactory.createMockYieldStream(usdt.address);
    stream = await MockYieldStream.at(tokentx.logs[0].args.addr);
    stream_token = usdt.address;
  }

  brief.write("Stream Token" , stream_token);
  brief.write("Stream",stream);

  console.log("creating env...");
  
  env = "0x73628a3695e0fBbfC4E9Ba88E576e27D977B35A9";
  owner = "0x1Ed79CEbC592044fF1e63A7a96dB944DB50e302D"//Todo
  await env.changeFeePoolAddr(owner)
  await env.changeWithdrawFeeRatio(10000);//Todo
  await env.transferOwnership(owner);
  brief.write("Env-xSushi", env.address);
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};

