const {DHelper, StepRecorder} = require("../util.js");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HGateKeeper = artifacts.require("HGateKeeper");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");
const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");
const TrustList = artifacts.require("TrustList");

async function performMigration(deployer, network, accounts) {
  //const data = fs.readFileSync(network + '-yusd-common.json', 'utf-8');
  //const common = JSON.parse(data.toString());
  sr =  new StepRecorder(network, "xSushi-2week-market");
  comm = StepRecorder(network, "xSushi-common")

  start_block = 14362470;//2022-3-11 9 a.m
  period = 83420;

  sr.write("network", network)
  sr.write("Start Block", start_block)
  sr.write("2week Period", period)

  stream_token = comm.value("Stream Token")
  env = comm.value("Env-xSushi")
  dispatcher = comm.value("Dispatcher")
  fee_pool_addr = "0x1Ed79CEbC592044fF1e63A7a96dB944DB50e302D"

  console.log("creating for 2week market");

  console.log("creating longterm...");
  hltfactory = await HLongTermFactory.deployed();
  //htfactory = await HTokenFactory.deployed();
  htfactory = await CachedHTokenFactory.deployed();
  tx = await hltfactory.createLongTerm(stream_token, start_block, period, htfactory.address);
  longterm = await HLongTerm.at(tx.logs[0].args.addr);
  //console.log("lt", longterm.address)
  sr.write("2week HLongTerm", longterm.address)

  console.log("creating gate keeper...");
  gatekeeper_factory = await HGateKeeperFactory.deployed();
  tx = await gatekeeper_factory.createGateKeeperForPeriod(env, dispatcher, longterm.address);
  gatekeeper = await HGateKeeper.at(tx.logs[2].args.addr)
  sr.write("2week HGateKeeper", gatekeeper.address)
  
  //await gas_pool.transferOwnership(gatekeeper.address);
  tlist = await TrustList.at(await htfactory.trustlist())
  tlist.add_trusted(gatekeeper.address);


  console.log("setting up support ratio");
  ratios = [76712, 115068, 153424, 191780, 230136, 268493, 306849, 345205, 383561, 421917, 460273, 498630, 536986, 575342, 613698, 652054, 690410, 728767, 767123, 805479]
  await gatekeeper.resetSupportRatios(ratios)
  console.log("setting yield interest pool")
  await gatekeeper.changeYieldPool(fee_pool_addr);

  longterms = {};
  console.log("generating longterm tokens");
  for (i = 0; i < ratios.length; i++){
    await longterm.getOrCreateLongTermToken(ratios[i])
    longterms[ratios[i]] = await HToken.at(await longterm.get_long_term_token_with_ratio(ratios[i]))
    str = "2week longterm token " + ratios[i].toString()
    sr.write(str, longterms[ratios[i]].address)
    console.log("longterm token ",i," ", longterms[ratios[i]].address)
    longterms[ratios[i]].transferOwnership(gatekeeper.address);
  }
  await longterm.getOrCreateLongTermToken(0)
  longterms[0] = await HToken.at(await longterm.get_long_term_token_with_ratio(0))
  str = "2week longterm token 0" 
  sr.write(str, longterms[0].address)
  longterms[0].transferOwnership(gatekeeper.address);

  console.log("transferring ownership...");
  await longterm.transferOwnership(gatekeeper.address);

  //todo link to minter
  //await gatekeeper.set_minter(mining.address);

  //await lt.addTransferListener(mining.address);
  //await ltf.addTransferListener(mining.address);
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
