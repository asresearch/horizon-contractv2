const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const AddressArray = artifacts.require("AddressArray");
const SafeMath = artifacts.require("SafeMath");
const {DHelper} = require("../util.js");


async function performMigration(deployer, network, accounts, dhelper) {
  await AddressArray.deployed();
  await SafeMath.deployed();
  await dhelper.readOrCreateContract(ERC20TokenFactory, [AddressArray], [SafeMath]);
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
