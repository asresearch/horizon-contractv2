### Horizon V2

1. To compile the contracts

`truffle compile`

2. Establish Ganache Network

`ganache-cli --host 127.0.0.1 --port 7545 --i 5777 -e 100000000000000-l 80000000000`

3. To migrate contracts to Ganache

`truffle migrate --network ganache`

4. Install testing modules

`npm install --save-dev sleep openzeppelin-test-helpers`

5. To run the tests

`truffle test --network ganache`


