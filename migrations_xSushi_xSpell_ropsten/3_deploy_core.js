const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HDispatcherFactory = artifacts.require("HDispatcherFactory");
const AddressArray = artifacts.require("AddressArray")
const SafeERC20 = artifacts.require("SafeERC20");
const SafeMath = artifacts.require("SafeMath");
const HEnvFactory = artifacts.require("HEnvFactory");
const HTokenFactory = artifacts.require("HTokenFactory");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const TrustList = artifacts.require("TrustList");
const TrustListFactory = artifacts.require("TrustListFactory");
const MockYieldStreamFactory = artifacts.require("MockYieldStreamFactory");
const MockYieldStream = artifacts.require("MockYieldStream");

const ContractPool = artifacts.require("ContractPool");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");
const TestHTokenCache = artifacts.require("TestHTokenCache");
const HGateKeeperHelper = artifacts.require("HGateKeeperHelper");

async function performMigration(deployer, network, accounts) {
  await AddressArray.deployed();
  await SafeMath.deployed();
  await SafeERC20.deployed();
  await deployer.link(AddressArray, TrustListFactory);
  await deployer.deploy(TrustListFactory);

  await deployer.deploy(HEnvFactory);

  await deployer.link(SafeMath, HGateKeeperHelper);
  await deployer.link(SafeERC20, HGateKeeperHelper);

  await deployer.deploy(HGateKeeperHelper);

  await deployer.link(SafeMath, HGateKeeperFactory);
  await deployer.link(SafeERC20, HGateKeeperFactory);
  await deployer.link(HGateKeeperHelper, HGateKeeperFactory);
  await deployer.deploy(HGateKeeperFactory);

  //await deployer.link(SafeMath, HDispatcherFactory);
  await deployer.link(SafeERC20, HDispatcherFactory);
  await deployer.deploy(HDispatcherFactory);

  await deployer.link(AddressArray, HTokenFactory)
  await deployer.link(SafeMath, HTokenFactory);
  await deployer.link(SafeERC20, HTokenFactory);
  await deployer.deploy(HTokenFactory);
  htoken_factory = await HTokenFactory.deployed();


  await deployer.link(SafeMath, HLongTermFactory);
  await deployer.link(SafeERC20, HLongTermFactory);
  await deployer.deploy(HLongTermFactory);

  tlfactory = await TrustListFactory.deployed();
  tokentx = await tlfactory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tokentx.logs[0].args.addr);


  await deployer.deploy(ContractPool, tlist.address);
  pool = await ContractPool.deployed();

  await deployer.link(AddressArray, CachedHTokenFactory)
  await deployer.deploy(CachedHTokenFactory, htoken_factory.address, pool.address, tlist.address);
  chtoken_factory = await CachedHTokenFactory.deployed();
  await tlist.add_trusted(chtoken_factory.address)

  await deployer.link(SafeERC20, MockYieldStreamFactory);
  await deployer.link(SafeMath, MockYieldStreamFactory);
  await deployer.deploy(MockYieldStreamFactory);
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};

