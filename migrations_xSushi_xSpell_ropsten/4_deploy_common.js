const HDispatcherFactory = artifacts.require("HDispatcherFactory");
const AddressArray = artifacts.require("AddressArray")
const SafeERC20 = artifacts.require("SafeERC20");
const SafeMath = artifacts.require("SafeMath");
const HEnvFactory = artifacts.require("HEnvFactory");
const {DHelper, StepRecorder} = require("../util.js");
const StdXSushi = artifacts.require("StdXSushi");
const StdXSpell = artifacts.require("StdXSpell");
const MockYieldStreamFactory = artifacts.require("MockYieldStreamFactory");
const MockYieldStream = artifacts.require("MockYieldStream");

const HGateKeeper = artifacts.require("HGateKeeper");
const HDispatcher = artifacts.require("HDispatcher");

const HEnv = artifacts.require("HEnv");

const fs = require("fs");

async function performMigration(deployer, network, accounts, dhelper) {
  if(network.includes("ropsten")){
    dispatcher = await HDispatcher.at("0xF91972d52922aC0f594757F1c81386201878898D")
  }
  else{
    dispatcher_factory = await HDispatcherFactory.deployed();
    tokentx = await dispatcher_factory.createHDispatcher();
    dispatcher = await HDispatcher.at(tokentx.logs[0].args.addr);
  }

  brief = StepRecorder(network, "xSushi-xSpell-common")
  brief.write("Dispatcher", dispatcher.address)

  await deployer.deploy(StdXSushi);
  xSushi = await StdXSushi.deployed();

  await deployer.deploy(StdXSpell);
  xSpell = await StdXSpell.deployed();

  mockfactory = await MockYieldStreamFactory.deployed();
  tokentx = await mockfactory.createMockYieldStream(xSushi.address);
  sushistream = await MockYieldStream.at(tokentx.logs[0].args.addr);
  await dispatcher.resetYieldStream(xSushi.address, sushistream.address);

  tokentx = await mockfactory.createMockYieldStream(xSpell.address);
  spellstream = await MockYieldStream.at(tokentx.logs[0].args.addr);
  await dispatcher.resetYieldStream(xSpell.address, spellstream.address);

  brief.write("xSushi-Token", xSushi.address);
  brief.write("xSushi-Stream",sushistream.address);
  brief.write("xSpell-Token", xSpell.address);
  brief.write("xSpell-Stream",spellstream.address);

  console.log("creating env...");
  envfactory = await dhelper.readOrCreateContract(HEnvFactory, []);
  tx = await envfactory.createHEnv(xSushi.address);
  sushienv = await HEnv.at(tx.logs[0].args.addr);
  brief.write("Env-sushi", sushienv.address);

  tx = await envfactory.createHEnv(xSpell.address);
  spellenv = await HEnv.at(tx.logs[0].args.addr);
  brief.write("Env-spell", spellenv.address);
  


}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};

