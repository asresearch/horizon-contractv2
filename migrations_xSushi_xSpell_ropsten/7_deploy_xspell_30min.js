const {DHelper, StepRecorder} = require("../util.js");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HGateKeeper = artifacts.require("HGateKeeper");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");
const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");
const TrustList = artifacts.require("TrustList");

async function performMigration(deployer, network, accounts) {
  //const data = fs.readFileSync(network + '-yusd-common.json', 'utf-8');
  //const common = JSON.parse(data.toString());
  comm = StepRecorder(network, "xSushi-xSpell-common");
  sr =  new StepRecorder(network, "xspell-30min-market");

  start_block = 11397500;//2021-11-10
  period = 100;

  sr.write("network", network)
  sr.write("Start Block", start_block)
  sr.write("30min Period", period)

  dispatcher = "0xF91972d52922aC0f594757F1c81386201878898D"
  fee_pool_addr = "0x91bB69fEa3d21F19FFE0a478fB6Ec8aA80904247"

  xspell = comm.value("xSpell-Token")
  spellenv = comm.value("Env-spell");
  console.log("creating xspell 30min market");

  console.log("creating longterm...");
  hltfactory = await HLongTermFactory.deployed();
  //htfactory = await HTokenFactory.deployed();
  htfactory = await CachedHTokenFactory.deployed();
  tx = await hltfactory.createLongTerm(xspell, start_block, period, htfactory.address);
  longterm = await HLongTerm.at(tx.logs[0].args.addr);
  sr.write("30min HLongTerm", longterm.address)

  console.log("creating gate keeper...");
  gatekeeper_factory = await HGateKeeperFactory.deployed();
  tx = await gatekeeper_factory.createGateKeeperForPeriod(spellenv, dispatcher, longterm.address);
  gatekeeper = await HGateKeeper.at(tx.logs[2].args.addr)
  sr.write("30min HGateKeeper", gatekeeper.address)
  
  //await gas_pool.transferOwnership(gatekeeper.address);
  tlist = await TrustList.at(await htfactory.trustlist())
  tlist.add_trusted(gatekeeper.address);

  console.log("setting up support ratio");
  ratios = [1000000,2000000,3000000,4000000,5000000,6000000,7000000,8000000,9000000,10000000,11000000,12000000,13000000,14000000,15000000,16000000,17000000,18000000,19000000,20000000]
  await gatekeeper.resetSupportRatios(ratios)
  console.log("setting yield interest pool")
  await gatekeeper.changeYieldPool(fee_pool_addr);

  longterms = {};
  console.log("generating longterm tokens");
  for (i = 0; i < 20; i++){
    await longterm.getOrCreateLongTermToken(ratios[i])
    longterms[ratios[i]] = await HToken.at(await longterm.get_long_term_token_with_ratio(ratios[i]))
    str = "xspell 30min longterm token " + ratios[i].toString()
    sr.write(str, longterms[ratios[i]].address)
    console.log("longterm token ", i ," ", longterms[ratios[i]].address)
    longterms[ratios[i]].transferOwnership(gatekeeper.address);
  }
  await longterm.getOrCreateLongTermToken(0)
  longterms[0] = await HToken.at(await longterm.get_long_term_token_with_ratio(0))
  str = "xspell 30min longterm token 0" 
  sr.write(str, longterms[0].address)
  longterms[0].transferOwnership(gatekeeper.address);

  console.log("transferring ownership...");
  await longterm.transferOwnership(gatekeeper.address);

  //todo link to minter
  //await gatekeeper.set_minter(mining.address);

  //await lt.addTransferListener(mining.address);
  //await ltf.addTransferListener(mining.address);
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
