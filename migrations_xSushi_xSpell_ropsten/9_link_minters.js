const {DHelper, StepRecorder} = require("../util.js");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HGateKeeper = artifacts.require("HGateKeeper");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");
const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");
const TrustList = artifacts.require("TrustList");


async function performMigration(deployer, network, accounts) {
  //const data = fs.readFileSync(network + '-yusd-common.json', 'utf-8');
  //const common = JSON.parse(data.toString());
  srm = StepRecorder(network, "30and60min_minter");
  sr11 = StepRecorder(network, "xsushi-30min-market");
  sr12 = StepRecorder(network, "xsushi-60min-market");
  sr21 = StepRecorder(network, "xspell-30min-market");
  sr22 = StepRecorder(network, "xspell-60min-market");
  gk11 = await HGateKeeper.at(sr11.value("30min HGateKeeper"))
  gk12 = await HGateKeeper.at(sr12.value("60min HGateKeeper"))
  gk21 = await HGateKeeper.at(sr21.value("30min HGateKeeper"))
  gk22 = await HGateKeeper.at(sr22.value("60min HGateKeeper"))
  minter11 = srm.value("sushi_mining30min")
  minter12 = srm.value("sushi_mining60min")
  minter21 = srm.value("spell_mining30min")
  minter22 = srm.value("spell_mining60min")
  await gk11.set_minter(minter11)
  await gk12.set_minter(minter12)
  await gk21.set_minter(minter21)
  await gk22.set_minter(minter22)

  ratios = [1000000,2000000,3000000,4000000,5000000,6000000,7000000,8000000,9000000,10000000,11000000,12000000,13000000,14000000,15000000,16000000,17000000,18000000,19000000,20000000]
  for (i = 0; i < 20; i++){
    str = "xsushi 30min longterm token " + ratios[i].toString()
    lt = await HToken.at(sr11.value(str))
    console.log("xsushi30min, ", i, "addr:", lt.address)
    await gk11.add_transfer_listener_to(lt.address, minter11);
    

    str = "xspell 30min longterm token " + ratios[i].toString()
    lt = await HToken.at(sr21.value(str))
    console.log("xspell30min, ", i, "addr:", lt.address)
    await gk21.add_transfer_listener_to(lt.address, minter21);
  }
  ratios = [2000000,4000000,6000000,8000000,10000000,12000000,14000000,16000000,18000000,20000000,22000000,24000000,26000000,28000000,30000000,32000000,34000000,36000000,38000000,40000000]
  for (i = 0; i < 20; i++){
    str = "xsushi 60min longterm token " + ratios[i].toString()
    lt = await HToken.at(sr12.value(str))
    console.log("xsushi60min, ", i, "addr:", lt.address)
    await gk12.add_transfer_listener_to(lt.address, minter12);

    str = "xspell 60min longterm token " + ratios[i].toString()
    lt = await HToken.at(sr22.value(str))
    console.log("xspell60min, ", i, "addr:", lt.address)
    await gk22.add_transfer_listener_to(lt.address, minter22);
  }

}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
