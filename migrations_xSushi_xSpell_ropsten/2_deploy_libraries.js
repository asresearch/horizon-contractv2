const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const {DHelper} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(SafeMath, [])
  await dhelper.readOrCreateContract(AddressArray, [])
  await dhelper.readOrCreateContract(Address, [])
  await dhelper.readOrCreateContract(SafeERC20, [])
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};

