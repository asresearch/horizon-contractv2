pragma solidity >=0.4.21 <0.6.0;
import "../core/HToken.sol";
import "../utils/Ownable.sol";

import "../utils/ContractPool.sol";

contract TestHTokenCache{
  HTokenFactoryInterface public factory;
  constructor(address _factory) public{
    factory = HTokenFactoryInterface(_factory);
  }

  event CHToken(address addr);
  function createHToken(string memory _name, string memory _sym) public{
    address a = factory.createHToken(_name, _sym, true);
    emit CHToken(a);
  }

  function destroyHToken(address addr) public{
    Ownable(addr).transferOwnership(address(factory));
    factory.destroyHToken(addr);
  }
}
