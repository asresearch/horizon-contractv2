pragma solidity >=0.4.21 <0.6.0;
import "../utils/Ownable.sol";
//import "../utils/SafeMath.sol";
//import "../erc20/SafeERC20.sol";
import "../erc20/IERC20.sol";
import "../ystream/IYieldStream.sol";
import "./HEnv.sol";
import "./HToken.sol";
import "./HGateKeeperHelper.sol";
import "./HGateKeeperParam.sol";
import "./HInterfaces.sol";

/// @notice Gatekeeper contains all user interfaces and updating values of tokens
contract HGateKeeper is Ownable{
  using SafeERC20 for IERC20;
  using SafeMath for uint256;
  using HGateKeeperHelper for HGateKeeperParam.settle_round_param_info;// mapping(uint256=>uint256) ;

  HDispatcherInterface public dispatcher;//the contracts to get the yield strem
  address public target_token;//the target token, e.g. yycurve
  HEnv public env;//the contract of environment variables, mainly the fee ratios

  HLongTermInterface public long_term;//the contract that maintain the period and generate/destroy all tokens.
  address public yield_interest_pool;//the pool that additional target tokens.

  MinterInterfaceGK public minter;

  uint256 public settled_round;//the index of the round that has been settled
  uint256 public max_amount;//the max amount that a wallet allowd to bid.


  mapping (uint256 => HGateKeeperParam.round_price_info) public round_prices;//price info of all rounds

  mapping (bytes32 => uint256) target_token_amount_in_round_and_ratio;//amount of target token invested in a given round and ratio
  mapping (bytes32 => uint256) long_term_token_amount_in_round_and_ratio;//amount of longterm token in a given round and ratio
  mapping (uint256 => uint256) total_target_token_in_round;//amount of total target token invested in a round

  /// @dev Constructor to create a gatekeeper
  /// @param _token_addr Address of the target token, such as yUSD or yvUSDC
  /// @param _env Address of env, to get fee ratios
  /// @param _dispatcher Address of the dispatcher, to get yield stream
  /// @param _long_term Address of the Hlongterm contract, to generate/destroy in/out-tokens.
  constructor(address _token_addr, address _env, address _dispatcher, address _long_term) public{
    target_token = _token_addr;
    env = HEnv(_env);
    dispatcher = HDispatcherInterface(_dispatcher);
    long_term = HLongTermInterface(_long_term);
    settled_round = 0;
  }

  event ChangeMaxAmount(uint256 old, uint256 _new);
  function set_max_amount(uint _amount) public onlyOwner{
    uint256 old = max_amount;
    max_amount = _amount;
    emit ChangeMaxAmount(old, max_amount);
  }

  event HorizonBid(address from, uint256 amount, uint256 share, uint256 ratio, address lp_token_addr);
  /// @dev User invests terget tokens to the contract to take part in the game in the next round and further rounds.
  /// User gets intokens with respect to the next round.
  /// @param _amount The amount of target token that the user invests
  /// @param _ratio The ratio that the user chooses.
  function bidRatio(uint256 _amount, uint256 _ratio) public returns(address lp_token_addr){
    require(_ratio == 0 || isSupportRatio(_ratio), "not support ratio");
    (address in_addr, bool created) = long_term.getOrCreateInToken(_ratio);

    _check_round();

    require(IERC20(target_token).allowance(msg.sender, address(this)) >= _amount, "not enough allowance");
    uint _before = IERC20(target_token).balanceOf(address(this));
    IERC20(target_token).safeTransferFrom(msg.sender, address(this), _amount);
    uint256 _after = IERC20(target_token).balanceOf(address(this));
    _amount = _after.safeSub(_before); // Additional check for deflationary tokens

    uint256 decimal = dispatcher.getYieldStream(target_token).getDecimal();
    require(decimal <= 1e18, "decimal too large");
    uint256 shares = _amount.safeMul(1e18).safeDiv(decimal);//turn into 1e18 decimal

    if(max_amount > 0){
      require(shares <= max_amount, "too large amount");
      require(IERC20(in_addr).balanceOf(msg.sender).safeAdd(shares) <= max_amount, "Please use another wallet");
    }

    HTokenInterfaceGK(in_addr).mint(msg.sender, shares);

    if (minter != MinterInterfaceGK(0x0)){
      MinterInterfaceGK(minter).handle_bid_ratio(msg.sender, shares, _ratio, long_term.getCurrentRound() + 1);
    }

    emit HorizonBid(msg.sender, _amount, shares, _ratio, in_addr);
    return in_addr;
  }

  function bidFloating(uint256 _amount) public returns(address lp_token_addr){
    return bidRatio(_amount, 0);  
  }
  event CancelBid(address from, uint256 amount, uint256 fee, address in_token_addr);
  function cancelBid(address _in_token_addr) public{
    bool is_valid = long_term.isPeriodInTokenValid(_in_token_addr);
    require(is_valid, "GK: invalid in token address");
    uint256 amount = IERC20(_in_token_addr).balanceOf(msg.sender);
    require(amount > 0, "GK: no bid at this period");

    _check_round();

    uint256 round = HTokenInterfaceGK(_in_token_addr).extra(keccak256("round"));
    uint256 _ratio = HTokenInterfaceGK(_in_token_addr).extra(keccak256("_ratio"));
    require(long_term.getCurrentRound() < round,
          "GK: round sealed already");
    //Todo minter

    HTokenInterfaceGK(_in_token_addr).burnFrom(msg.sender, amount);

    uint256 decimal = dispatcher.getYieldStream(target_token).getDecimal();

    uint256 target_amount = amount.safeMul(decimal).safeDiv(1e18);

    if (minter != MinterInterfaceGK(0x0)){
      MinterInterfaceGK(minter).handle_cancel_bid(msg.sender, amount, _ratio, long_term.getCurrentRound() + 1);
    }

    if(env.cancel_fee_ratio() != 0 && env.fee_pool_addr() != address(0x0)){
      uint256 fee = target_amount.safeMul(env.cancel_fee_ratio()).safeDiv(env.ratio_base());
      uint256 recv = target_amount.safeSub(fee);
      IERC20(target_token).safeTransfer(msg.sender, recv);
      IERC20(target_token).safeTransfer(env.fee_pool_addr(), fee);
      emit CancelBid(msg.sender, recv, fee, _in_token_addr);
    }else{
      IERC20(target_token).safeTransfer(msg.sender, target_amount);
      emit CancelBid(msg.sender, target_amount, 0, _in_token_addr);
    }
  }
   /*
  function changeBid(address _in_token_addr, uint256 _new_amount, uint256 _new_ratio) public{
    cancelBid(_in_token_addr);
    bidRatio(_new_amount, _new_ratio);
  }*/

  event HorizonWithdrawLongTermToken(address from, uint256 amount, uint256 ratio, address lp_token_addr);
  /// @dev User changes longterm tokens to outtokens with respect to the next round,
  /// meaning that he quits the game by the next round.
  /// @param _long_term_token_addr The address of the longterm token that the user wants to withdraw
  /// @param _amount The amount of longterm token that the user wants to withdraw.
  function withdrawLongTerm(address _long_term_token_addr, uint256 _amount) public{
    require(long_term.isLongTermTokenValid(_long_term_token_addr), "GK: invalid long term token address");
    uint256 ratio = HTokenInterfaceGK(_long_term_token_addr).extra(keccak256("ratio"));
    (address out_addr, ) = long_term.getOrCreateOutToken(ratio);

    _check_round();

    require(IERC20(_long_term_token_addr).balanceOf(msg.sender) >= _amount, "GK:not enough amount");

    HTokenInterfaceGK(_long_term_token_addr).burnFrom(msg.sender, _amount);
    HTokenInterfaceGK(out_addr).mint(msg.sender, _amount);
    if (minter != MinterInterfaceGK(0x0)){
      MinterInterfaceGK(minter).handle_withdraw(msg.sender, _amount, ratio, long_term.getCurrentRound() + 1);
    }

    emit HorizonWithdrawLongTermToken(msg.sender, _amount, ratio, out_addr);
  }


  function withdrawInToken(address _in_token_addr, uint256 _amount) public{
    bool is_valid = long_term.isPeriodInTokenValid(_in_token_addr);
    require(is_valid, "GK: invalid in token address");
    _check_round();

    HTokenInterfaceGK in_token = HTokenInterfaceGK(_in_token_addr);
    uint256 round = in_token.extra(keccak256("round"));
    require(long_term.getCurrentRound() >= round, "GK: round not sealed");
    require(in_token.ratio_to_target() > 0, "GK: some thing wrong 2");
    uint256 amount = IERC20(_in_token_addr).balanceOf(msg.sender);

    require(_amount <= amount, "GK: not enough intoken balance");
    in_token.burnFrom(msg.sender, _amount);

    if(IERC20(_in_token_addr).totalSupply() == 0){
      Ownable(_in_token_addr).transferOwnership(address(long_term.token_factory()));
      long_term.token_factory().destroyHToken(_in_token_addr);
    }

    uint256 lt_amount = _amount.safeMul(in_token.ratio_to_target()).safeDiv(1e18);
    uint256 ratio = in_token.extra(keccak256("ratio"));

    emit HorizonExchangeToLongTermToken(msg.sender, _in_token_addr, _amount, lt_amount, ratio);

    (address out_addr, ) = long_term.getOrCreateOutToken(ratio);
    HTokenInterfaceGK(out_addr).mint(msg.sender, lt_amount);
    if (minter != MinterInterfaceGK(0x0)){
      MinterInterfaceGK(minter).handle_withdraw(msg.sender, lt_amount, ratio, long_term.getCurrentRound() + 1);
    }

    emit HorizonWithdrawLongTermToken(msg.sender, lt_amount, ratio, out_addr);
  }
  event HorizonCancelWithdraw(address from, uint256 amount, uint256 fee, address out_token_addr);
  /// @dev User cancel his/her withdraw operation,
  /// changing all outtokens back to longterm token.
  /// @param _out_token_addr The address of outtoken.
  function cancelWithdraw(address _out_token_addr) public{
    require(long_term.isPeriodOutTokenValid(_out_token_addr), "GK: invalue out token address");
    uint256 amount = IERC20(_out_token_addr).balanceOf(msg.sender);
    require(amount > 0, "GK: no bid at this period");

    _check_round();

    uint256 round = HTokenInterfaceGK(_out_token_addr).extra(keccak256("round"));
    require(long_term.getCurrentRound() < round,
           "round sealed already");
    //if (long_term.getCurrentRound() >= period) return;
    uint256 ratio = HTokenInterfaceGK(_out_token_addr).extra(keccak256("ratio"));
    address long_term_token_addr = long_term.get_long_term_token_with_ratio(ratio);

    HTokenInterfaceGK(_out_token_addr).burnFrom(msg.sender, amount);
    HTokenInterfaceGK(long_term_token_addr).mint(msg.sender, amount);
    if (minter != MinterInterfaceGK(0x0)){
      MinterInterfaceGK(minter).handle_cancel_withdraw(msg.sender, amount, ratio, long_term.getCurrentRound() + 1);
    }

    emit HorizonCancelWithdraw(msg.sender, amount, 0, _out_token_addr);
  }



  event HorizonClaim(address from, address _out_token_addr, uint256 amount, uint256 fee);
  /// @dev User withdraws outtokens to get target tokens.
  /// @param _out_token_addr The address of outtoken.
  /// @param _amount The amount of outtoken.
  function claim(address _out_token_addr, uint256 _amount) public {
    bool is_valid = long_term.isPeriodOutTokenValid(_out_token_addr);
    require(is_valid, "GK: invalid out token address");
    uint256 amount = IERC20(_out_token_addr).balanceOf(msg.sender);
    require(amount >= _amount, "GK: no enough bid at this period");

    _check_round();
    HTokenInterfaceGK out_token = HTokenInterfaceGK(_out_token_addr);
    require(long_term.isRoundEnd(out_token.extra(keccak256("round")) - 1), "GK: period not end");
    require(out_token.ratio_to_target() > 0, "GK: something wrong 1");

    uint256 decimal = dispatcher.getYieldStream(target_token).getDecimal();
    uint256 t = _amount.safeMul(out_token.ratio_to_target()).safeMul(decimal).safeDiv(1e36);//turn into target decimal

    out_token.burnFrom(msg.sender, _amount);

    if(env.withdraw_fee_ratio() != 0 && env.fee_pool_addr() != address(0x0)){
      uint256 fee = t.safeMul(env.withdraw_fee_ratio()).safeDiv(env.ratio_base());
      uint256 recv = t.safeSub(fee);
      IERC20(target_token).safeTransfer(msg.sender, recv);
      IERC20(target_token).safeTransfer(env.fee_pool_addr(), fee);
      emit HorizonClaim(msg.sender, _out_token_addr, recv, fee);
    }else{
      IERC20(target_token).safeTransfer(msg.sender, t);
      emit HorizonClaim(msg.sender, _out_token_addr, t, 0);
    }

    if(IERC20(_out_token_addr).totalSupply() == 0){
      Ownable(_out_token_addr).transferOwnership(address(long_term.token_factory()));
      long_term.token_factory().destroyHToken(_out_token_addr);
    }
  }

  event HorizonExchangeToLongTermToken(address from, address in_token_addr, uint256 amount_in, uint256 amount_long, uint256 ratio);
  /// @dev User changes all intokens to long-term tokens,
  /// so that the user can withdraw to outtoken or transfer in secondary markets.
  /// @param _in_token_addr The address of intoken.
  function exchangeToLongTermToken(address _in_token_addr) public{
    bool is_valid = long_term.isPeriodInTokenValid(_in_token_addr);
    require(is_valid, "GK: invalid in token address");
    _check_round();

    HTokenInterfaceGK in_token = HTokenInterfaceGK(_in_token_addr);
    uint256 round = in_token.extra(keccak256("round"));
    require(long_term.getCurrentRound() >= round, "GK: round not sealed");
    require(in_token.ratio_to_target() > 0, "GK: some thing wrong 2");
    uint256 amount = IERC20(_in_token_addr).balanceOf(msg.sender);
    require(amount > 0, "GK: no in token balance");
    in_token.burnFrom(msg.sender, amount);
    uint256 rec = amount.safeMul(in_token.ratio_to_target()).safeDiv(1e18);
    uint256 ratio = in_token.extra(keccak256("ratio"));
    HTokenInterfaceGK long_term_token = HTokenInterfaceGK(long_term.get_long_term_token_with_ratio(ratio));
    long_term_token.mint(msg.sender, rec);
    emit HorizonExchangeToLongTermToken(msg.sender, _in_token_addr, amount, rec, ratio);

    if(IERC20(_in_token_addr).totalSupply() == 0){
      Ownable(_in_token_addr).transferOwnership(address(long_term.token_factory()));
      long_term.token_factory().destroyHToken(_in_token_addr);
    }
  }

  /// @dev To check whether the current round should end.
  /// If so, do settlement for the current round and begin a new round.
  HGateKeeperParam.settle_round_param_info info;
  function _check_round() internal{
    long_term.updatePeriodStatus();
    uint256 new_period = long_term.getCurrentRound();
    if(round_prices[new_period].start_price == 0){
      round_prices[new_period].start_price = dispatcher.getYieldStream(target_token).getVirtualPrice();
    }
    if(long_term.isRoundEnd(settled_round + 1)){
      /*HGateKeeperParam.settle_round_param_info memory info*/ info = HGateKeeperParam.settle_round_param_info({
                      _round:settled_round+1,
                       dispatcher:dispatcher,
                       target_token:target_token,
                       minter:minter,
                       long_term:long_term,
                       sratios:sratios,
                       env_ratio_base:env.ratio_base(),
                       yield_interest_pool:yield_interest_pool,
                       start_price:round_prices[settled_round+1].start_price,
                       end_price:round_prices[settled_round+1].end_price,
                       total_target_token:total_target_token_in_round[settled_round+1],
                       total_target_token_next_round:total_target_token_in_round[settled_round+2]
      });
      settled_round =
        info.settle_round(target_token_amount_in_round_and_ratio, long_term_token_amount_in_round_and_ratio);
      total_target_token_in_round[settled_round + 1] = total_target_token_in_round[settled_round + 1].safeAdd(info.total_target_token_next_round);
    }
  }

  mapping (uint256 => bool) public support_ratios;
  uint256[] public sratios;

  event SupportRatiosChanged(uint256[] rs);
  function resetSupportRatios(uint256[] memory rs) public onlyOwner{
    for(uint i = 0; i < sratios.length; i++){
      delete support_ratios[sratios[i]];
    }
    delete sratios;
    for(uint i = 0; i < rs.length; i++){
      if(i > 0){
        require(rs[i] > rs[i-1], "should be ascend");
      }
      sratios.push(rs[i]);
      support_ratios[rs[i]] = true;
    }
    emit SupportRatiosChanged(sratios);
  }

  function isSupportRatio(uint256 r) public view returns(bool){
    for(uint i = 0; i < sratios.length; i++){
      if(sratios[i] == r){
        return true;
      }
    }
    return false;
  }
  function updatePeriodStatus() public{
    _check_round();
  }


  event ChangeYieldInterestPool(address old, address _new);
  function changeYieldPool(address _pool) onlyOwner public{
    require(_pool != address(0x0), "invalid pool");
    address old = yield_interest_pool;
    yield_interest_pool = _pool;
    emit ChangeYieldInterestPool(old, _pool);
  }
  event SetMinter(address addr);
  function set_minter(address addr) onlyOwner public{
    minter = MinterInterfaceGK(addr);
    emit SetMinter(addr);
  }
  
  function add_transfer_listener_to(address _lt_token, address _listener) onlyOwner public{
    require(long_term.isLongTermTokenValid(_lt_token), "GK: invalid long term token address");
    HTokenInterfaceGK(_lt_token).addTransferListener(_listener);
  }
  function remove_transfer_listener_to(address _lt_token, address _listener) onlyOwner public{
    require(long_term.isLongTermTokenValid(_lt_token), "GK: invalid long term token address");
    HTokenInterfaceGK(_lt_token).removeTransferListener(_listener);
  }

}

contract HGateKeeperFactory is Ownable{
  event NewGateKeeper(address addr);

  function createGateKeeperForPeriod(address _env_addr, address _dispatcher, address _long_term) public returns(address){
    HEnv e = HEnv(_env_addr);
    HGateKeeper gk = new HGateKeeper(e.token_addr(), _env_addr, _dispatcher, _long_term);
    gk.transferOwnership(msg.sender);
    emit NewGateKeeper(address(gk));
    return address(gk);
  }
}

