pragma solidity >=0.4.21 <0.6.0;
import "../utils/Ownable.sol";
import "../utils/SafeMath.sol";
import "../erc20/SafeERC20.sol";
import "../erc20/ERC20Impl.sol";
import "./HToken.sol";

import "./HPeriod.sol";


contract HTokenInterfaceLT{
  function mint(address addr, uint256 amount)public;
  function burnFrom(address addr, uint256 amount) public;
  function set_ratio_to_target(uint256 _balance) public;
  function set_extra(bytes32 _target, uint256 _value) public;
  function set_target(address _target) public;
  mapping (bytes32 => uint256) public extra;
  uint256 public ratio_to_target;
  function transferOwnership(address addr) public;
}
/// @notice Longterm contract maintain the period and generate/destroy all tokens.
contract HLongTerm is HPeriod, Ownable{

  //this struct maps the token info, including the correspoinding target token (the market),
  // the period (e.g., 1 week, 2 weeks), the ratio, the round and the type ("in", "out" or "long")
  // to the address of the token
  struct round_token_info{
    mapping(bytes32 => address) hash_to_tokens;
  }

  mapping (uint256 => round_token_info) all_round_tokens;//recording the information in a round
  mapping (uint256 => address) long_term_tokens;//maps the ratio to correspounding longterm token address
  mapping (address => bool) long_term_token_bool;//identidy that whether an address is a valie longterm address

  HTokenFactoryInterface public token_factory;//the factory for all types of token
  address public target_token;//the address of target token, e.g. yycurve


  constructor(address _target_token, uint256 _start_block, uint256 _period, address _token_factory)
    HPeriod(_start_block, _period) public{
    target_token = _target_token;
    token_factory = HTokenFactoryInterface(_token_factory);
  }

  function uint2str(uint256 i) internal pure returns (string memory c) {
    if (i == 0) return "0";
    uint256 j = i;
    uint256 length;
    while (j != 0){
        length++;
        j /= 10;
    }
    bytes memory bstr = new bytes(length);
    uint256 k = length - 1;
    while (i != 0){
      bstr[k--] = byte(48 + uint8(i % 10));
      i /= 10;
    }
    c = string(bstr);
  }

  function get_long_term_token_with_ratio(uint256 _ratio) public view returns(address){
    return long_term_tokens[_ratio];
  }

  function _getOrCreateToken(uint ratio, string memory in_out, string memory prefix, uint256 _type) internal returns(address, bool){
    if(_type != 3){
      _end_current_and_start_new_round();
    }
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), ratio, getCurrentRound() + 1, in_out));

    round_token_info storage pi = all_round_tokens[getCurrentRound() + 1];
    if(pi.hash_to_tokens[h] == address(0x0)){
      string memory _postfix = uint2str(getParamPeriodBlockNum());
      string memory name;
      string memory symbol;
      if (_type != 3) {
        name = string(abi.encodePacked("horizon_", in_out, "_", ERC20Base(target_token).name(), "_", _postfix, "_", ratio > 0 ? uint2str(ratio) : "floating", "_", uint2str(getCurrentRound() + 1)));
        symbol = string(abi.encodePacked(prefix, ERC20Base(target_token).symbol(), "_", _postfix, "_", ratio > 0 ? uint2str(ratio) : "f", "w", uint2str(getCurrentRound() + 1)));
      } else {
        name = string(abi.encodePacked("horizon_", in_out, "_", ERC20Base(target_token).name(), "_", _postfix, "_", ratio > 0 ? uint2str(ratio) : "floating"));
        symbol = string(abi.encodePacked(prefix, ERC20Base(target_token).symbol(), "_", _postfix, "_", ratio > 0 ? uint2str(ratio) : "f"));
      }
      HTokenInterfaceLT pt = HTokenInterfaceLT(token_factory.createHToken(name, symbol, _type == 3));
      if(_type != 3){
        pt.set_extra(keccak256("round"), getCurrentRound() + 1);
      }
      pt.set_extra(keccak256("ratio"), ratio);
      pt.set_extra(keccak256("type"), _type);
      pt.set_target(long_term_tokens[ratio]);

      pt.transferOwnership(msg.sender);
      if (_type != 3){
      pi.hash_to_tokens[h] = address(pt);
      }
      else{
        long_term_tokens[ratio] = address(pt);
        long_term_token_bool[address(pt)] = true;
      }
      return (pi.hash_to_tokens[h], true);
    }
    return (pi.hash_to_tokens[h], false);
  }

  function getOrCreateInToken(uint ratio) public onlyOwner returns(address, bool){
    return _getOrCreateToken(ratio, "in", "hi", 1);
  }

  function getOrCreateOutToken(uint ratio) public onlyOwner returns(address, bool){
    return _getOrCreateToken(ratio, "out", "ho", 2);
  }


  function getOrCreateLongTermToken(uint ratio) public onlyOwner returns(address, bool){
    return _getOrCreateToken(ratio, "long", "hl", 3);
  }

  function updatePeriodStatus() public onlyOwner returns(bool){
    return _end_current_and_start_new_round();
  }

  function isLongTermTokenValid(address _addr) public view returns(bool){
    return long_term_token_bool[_addr];
  }

  function isPeriodInTokenValid(address _token_addr) public view returns(bool){
    if (_token_addr == address(0)) return false;
    HTokenInterfaceLT hti = HTokenInterfaceLT(_token_addr);
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), hti.extra(keccak256("ratio")), hti.extra(keccak256("round")),"in"));
    round_token_info storage pi = all_round_tokens[hti.extra(keccak256("round"))];
    if(pi.hash_to_tokens[h] == _token_addr){
      return true;
    }
    return false;
  }
  function isPeriodOutTokenValid(address _token_addr) public view returns(bool){
    if (_token_addr == address(0)) return false;
    HTokenInterfaceLT hto = HTokenInterfaceLT(_token_addr);
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), hto.extra(keccak256("ratio")), hto.extra(keccak256("round")),"out"));
    round_token_info storage pi = all_round_tokens[hto.extra(keccak256("round"))];
    if(pi.hash_to_tokens[h] == _token_addr){
      return true;
    }
    return false;
  }

  function totalInAtPeriodWithRatio(uint256 _period, uint256 _ratio) public view returns(uint256) {
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), _ratio, _period,"in"));
    round_token_info storage pi = all_round_tokens[_period];
    address c = pi.hash_to_tokens[h];
    if(c == address(0x0)) return 0;

    IERC20 e = IERC20(c);
    return e.totalSupply();
  }

  function hintokenAtPeriodWithRatio(uint256 _period, uint256 _ratio) public view returns(address){
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), _ratio, _period,"in"));
    round_token_info storage pi = all_round_tokens[_period];
    address c = pi.hash_to_tokens[h];
    return c;
  }

  function totalOutAtPeriodWithRatio(uint256 _period, uint256 _ratio) public view returns(uint256) {
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), _ratio, _period, "out"));
    round_token_info storage pi = all_round_tokens[_period];
    address c = pi.hash_to_tokens[h];
    if(c == address(0x0)) return 0;

    IERC20 e = IERC20(c);
    return e.totalSupply();
  }

  function houttokenAtPeriodWithRatio(uint256 _period, uint256 _ratio) public view returns(address){
    bytes32 h = keccak256(abi.encodePacked(target_token, getParamPeriodBlockNum(), _ratio, _period,"out"));
    round_token_info storage pi = all_round_tokens[_period];
    address c = pi.hash_to_tokens[h];
    return c;
  }
}

contract HLongTermFactory{

  event NewLongTerm(address addr);
  function createLongTerm(address _target_token, uint256 _start_block, uint256 _period, address _token_factory) public returns(address){
    HLongTerm pt = new HLongTerm(_target_token, _start_block, _period, _token_factory);

    pt.transferOwnership(msg.sender);
    emit NewLongTerm(address(pt));
    return address(pt);
  }
}


