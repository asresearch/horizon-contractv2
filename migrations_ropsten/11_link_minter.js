const {DHelper, StepRecorder} = require("./util.js");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HGateKeeper = artifacts.require("HGateKeeper");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");
const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");
const TrustList = artifacts.require("TrustList");


async function performMigration(deployer, network, accounts) {
  //const data = fs.readFileSync(network + '-yusd-common.json', 'utf-8');
  //const common = JSON.parse(data.toString());
  sr1 =  new StepRecorder(network, "30min market");
  sr2 =  new StepRecorder(network, "60min market");
  gk1 = await HGateKeeper.at(sr1.value("30min HGateKeeper"))
  gk2 = await HGateKeeper.at(sr2.value("60min HGateKeeper"))
  minter1 = "0x29a38cD4eaf5832c44c805Dff8BaA0f75cdaF059"
  minter2 = "0xc43Bc1c09f794147CFA23b48c589fB92C1aFedd9"
  await gk1.set_minter(minter1)
  await gk2.set_minter(minter2)

  ratios = [1000000,2000000,3000000,4000000,5000000,6000000,7000000,8000000,9000000,10000000,11000000,12000000,13000000,14000000,15000000,16000000,17000000,18000000,19000000,20000000]
  for (i = 0; i < 20; i++){
    str = "30min longterm token " + ratios[i].toString()
    lt = await HToken.at(sr1.value(str))
    await gk1.add_transfer_listener_to(lt.address, minter1);
  }
  ratios = [2000000,4000000,6000000,8000000,10000000,12000000,14000000,16000000,18000000,20000000,22000000,24000000,26000000,28000000,30000000,32000000,34000000,36000000,38000000,40000000]
  for (i = 0; i < 20; i++){
    str = "60min longterm token " + ratios[i].toString()
    lt = await HToken.at(sr2.value(str))
    await gk2.add_transfer_listener_to(lt.address, minter2);
  }

}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
