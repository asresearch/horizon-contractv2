const fs = require("fs");
const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HGateKeeper = artifacts.require("HGateKeeper");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");
const CachedHTokenFactory = artifacts.require("CachedHTokenFactory");


async function performMigration(deployer, network, accounts) {
  const data = fs.readFileSync(network + '-xsushi-common.json', 'utf-8');
  const common = JSON.parse(data.toString());

  if(network.includes("ropsten") ||
  network.includes("ganache")){
    start_block = 123456;
    period = 100;
  }else if(network.includes("main")){
    start_block = 11690000;
    period = 41710;
  }

  brief = {}
  brief["network"] = network
  brief["Start Block"] = start_block
  brief["Period"] = period

  if(network.includes("main")){
    stream_token = common["xSushi Stream Token"]
    env = common["Env"]
    dispatcher = common["Dispatcher"]
    fee_pool_addr = common["Fee pool"]

    console.log("creating for 1 week yusd market");

    console.log("creating period token...");
    hltfactory = await HLongTermFactory.deployed();
    //htfactory = await HTokenFactory.deployed();
    htfactory = await CachedHTokenFactory.deployed();

    tx = await hltfactory.createLongTerm(stream_token, start_block, period, htfactory.address);
    hlt = await HLongTerm.at(tx.logs[0].args.addr);
    brief["HLongTerm"] = hlt.address;

    console.log("creating gate keeper...");
    gatekeeper_factory = await HGateKeeperFactory.deployed();
    tx = await gatekeeper_factory.createGateKeeperForPeriod(env, dispatcher, hlt.address);
    gatekeeper = await HGateKeeper.at(tx.logs[2].args.addr)
    brief["HGateKeeper"] = gatekeeper.address;

    console.log("transfer ownership...");
    await hlt.transferOwnership(gatekeeper.address);
  //await gas_pool.transferOwnership(gatekeeper.address);
  }

  if(network.includes("main")){
    console.log("setting up support ratio");
    await gatekeeper.resetSupportRatios([95890, 191780, 287671, 383561, 479452, 575342, 671232, 767123, 863013, 958904, 1054794, 1150684, 1246575, 1342465, 1438356, 1534246, 1630136, 1726027, 1821917, 1917808])
    console.log("setting yield interest pool")
    await gatekeeper.changeYieldPool(fee_pool_addr);
  }

  const wdata = JSON.stringify(brief);
  await fs.writeFile(network+'-1week-xsushi.json', wdata, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
  });

}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
