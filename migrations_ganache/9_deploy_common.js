const HGateKeeperFactory = artifacts.require("HGateKeeperFactory");
const HGateKeeper = artifacts.require("HGateKeeper");
const HDispatcherFactory = artifacts.require("HDispatcherFactory");
const HDispatcher = artifacts.require("HDispatcher");
const AddressArray = artifacts.require("AddressArray")
//const USDT = artifacts.require("USDT");
const STDERC20 = artifacts.require("STDERC20");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const MockYieldStreamFactory = artifacts.require("MockYieldStreamFactory");
const MockYieldStream = artifacts.require("MockYieldStream");
const SafeERC20 = artifacts.require("SafeERC20");
const SafeMath = artifacts.require("SafeMath");
//const TokenBank = artifacts.require("TokenBank");
//const TokenBankFactory = artifacts.require("TokenBankFactory");
const HLongTermFactory = artifacts.require("HLongTermFactory");
const HLongTerm = artifacts.require("HLongTerm");

const HEnvFactory = artifacts.require("HEnvFactory");
const HEnv = artifacts.require("HEnv");

const HToken = artifacts.require("HToken");
const HTokenFactory = artifacts.require("HTokenFactory");
const yUSDStream = artifacts.require("yUSDStream");
const xSushiStream = artifacts.require("xSushiStream")

const fs = require("fs");

async function performMigration(deployer, network, accounts) {

  brief = {}

  console.log("creating dispatcher...");
  brief["network"] = network;
  dispatcher_factory = await HDispatcherFactory.deployed();
  tokentx = await dispatcher_factory.createHDispatcher();
  dispatcher = await HDispatcher.at(tokentx.logs[0].args.addr);

  brief["Dispatcher"] = dispatcher.address

  if(network.includes("ropsten") ||
  network.includes("ganache")){
    await deployer.deploy(STDERC20);
    usdt = await STDERC20.deployed();

    await deployer.link(SafeERC20, MockYieldStreamFactory);
    await deployer.link(SafeMath, MockYieldStreamFactory);

    await deployer.deploy(MockYieldStreamFactory);
    mockfactory = await MockYieldStreamFactory.deployed();
    tokentx = await mockfactory.createMockYieldStream(usdt.address);
    stream = await MockYieldStream.at(tokentx.logs[0].args.addr);
    stream_token = usdt.address;

  }else if(network.includes("main")){
    //await deployer.link(SafeMath, xSushiStream);
    //await deployer.deploy(xSushiStream);
    await deployer.deploy(yUSDStream);
    stream = await yUSDStream.deployed();
    stream_token = await stream.target_token();
  }
  m = await SafeMath.deployed();
  brief["SafeMath"] = m.address;
  e = await SafeERC20.deployed();
  brief["SafeERC20"] = e.address;

  await dispatcher.resetYieldStream(stream_token, stream.address);
  brief["Stream Token"] = stream_token;
  brief["Stream"] = stream.address;

  console.log("creating trust list...");
  tlfactory = await TrustListFactory.deployed();
  tokentx = await tlfactory.createTrustList(['0x0000000000000000000000000000000000000000']);
  gas_pool_tlist = await TrustList.at(tokentx.logs[0].args.addr);
  tokentx = await tlfactory.createTrustList(['0x0000000000000000000000000000000000000000']);
  fee_pool_tlist = await TrustList.at(tokentx.logs[0].args.addr);


  console.log("creating token bank...");
  //tkfactory = await TokenBankFactory.deployed();
  //tx = await tkfactory.newTokenBank("Horizon Fee Pool", stream_token, fee_pool_tlist.address);
  //fee_pool = await TokenBank.at(tx.logs[0].args.addr);

  brief["Fee pool trust list"] = fee_pool_tlist.address;
  //brief['Fee pool'] = fee_pool.address;

  console.log("creating env...");
  envfactory = await HEnvFactory.deployed();
  tx = await envfactory.createHEnv(stream_token);
  env = await HEnv.at(tx.logs[0].args.addr);
  //await env.changeFeePoolAddr(fee_pool.address);
  brief["Env"] = env.address;


  for(var key in brief){
    console.log(key, " ", brief[key]);
  }
// write JSON string to a file
  const data = JSON.stringify(brief);
  await fs.writeFile(network+'-yusd-common.json', data, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
  });
}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
